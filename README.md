# Stedroids

## Rules

[Read the rules here.](https://gitlab.com/dennuguyen/stedroids/-/issues/3)

## Problem Statement

For the upcoming Droid Racing Challenge our team hopes to produce a fully wireless robot to traverse a track in the fastest time for the university division, while utilising computer vision to sense boundaries, obstacles, and other droids. During the race the droid will also complete the turning challenge where it will be required to choose the correct direction to take at a fork based on a provided left or right-hand turn sign. All these requirements must be met without the robot exceeding the dimensions of 400 mm x 350 mm x 800 mm and a total cost of $1500.

## Budget

| Name | Quantity | Price Per Item (AUD) | Link |
| --- | --- | --- | --- |
| LattePanda Delta | 1 | 384.95 | https://core-electronics.com.au/lattepanda-delta-4g-32gb-tiny-ultimate-windows-linux-device-pre-order.html |
| LattePanda V1 | 1 | 294.95 | https://core-electronics.com.au/lattepanda-4g-64gb-the-most-powerful-win10-dev-board.html |
| QUICRUN 3650 G1 Sensored 10.5T Motor | 2 | 82.80 | https://www.campbelltownhobbies.com.au/quicrun-3650-g2-sensored-10.5t-motor |
| QUICRUN-WB 10BL60 | 2 | 68.32 | https://hobbyking.com/en_us/quicrun-wp-10bl60.html |
| Tamiya Ta-06 Chassis | 1 | 250 | https://www.tamiya.com/english/products/84378/index.htm |
| Tornado RC LiPo 4200mAh 7.4V 2S | 1 | 58.29 | https://www.campbelltownhobbies.com.au/tornado-rc-lipo-4200mah-45c-round-7-4v-2s-deans |
| Intel RealSense D435 Depth Camera | 1 | 300 | https://www.jw.com.au/intel-realsense-d435depth-camera-usb-rgb-sensor-478607 |
| 4200 mAh LiPo 7.6 V | 1 | 53.73 | https://www.aliexpress.com/item/1005001338934817.html |
| BNO055 9-DOF IMU | 1 | 65.90 | https://core-electronics.com.au/adafruit-9-dof-absolute-orientation-imu-fusion-breakout-bno055.html |


## Resources

- Controlling self-driving cars: https://youtu.be/4Y7zG48uHRo
- D400 Series Datasheet: https://www.intel.com/content/dam/support/us/en/documents/emerging-technologies/intel-realsense-technology/Intel-RealSense-D400-Series-Datasheet.pdf
- QUICRUN Motor User Manual: https://www.hobbywing.com/products/enpdf/QuicRun%203650G2.pdf
- LattePanda Delta Documentation: http://docs.lattepanda.com/content/delta_edition/get_started/
- LattePanda V1 Documentation: http://docs.lattepanda.com/content/1st_edition/power_on/
- QUICKRUN-WP 10BL60: https://www.hobbywing.com/products/enpdf/QuicRunWP10BL30-10BL60-8BL150.pdf
- BNO055 Documentation: https://cdn-learn.adafruit.com/downloads/pdf/adafruit-bno055-absolute-orientation-sensor.pdf
