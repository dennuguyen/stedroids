function [points, colours] = get_pointcloud()
    global realsense_camera_pipe;

    % Create a point cloud object.
    pointcloud = realsense.pointcloud();

    % Get the frame.
    frame = realsense_camera_pipe.wait_for_frames();

    % Align the Realsense's cameras to the RGB camera then filter out the
    % frame.
    aligned_frame = realsense.align(realsense.stream.color).process(frame);

    % Split the frame into depth and colour frames.
    depth_frame = aligned_frame.get_depth_frame();
    colour_frame = aligned_frame.get_color_frame();

    % Get the point cloud from the depth frame.
    points = pointcloud.calculate(depth_frame).get_vertices();

    % Associate each point in the point cloud to a colour.
    pointcloud.map_to(colour_frame);

    % Get the colour data from the colour frame.
    colour_data = colour_frame.get_data();
    colours = [colour_data(1:3:end)', colour_data(2:3:end)', colour_data(3:3:end)'];
end
