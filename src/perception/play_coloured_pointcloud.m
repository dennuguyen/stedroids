function play_coloured_pointcloud(point_cloud, rgb_cloud)
    % Video player to view the frames.
    global realsense_camera_player;

    if isempty(realsense_camera_player)
        realsense_camera_player = pcplayer([-1 1], [-1 1], [-1 1]);
    end

    % Display the coloured point cloud.
    if isOpen(realsense_camera_player)
        view(realsense_camera_player, point_cloud, rgb_cloud);
    end
end