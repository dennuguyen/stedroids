function pointcloud = extract_colour(pointcloud, hue_range, saturation_range, value_range)
    % Get the HSV conversion of the pointcloud.
    hsv_points = rgb2hsv(mat2gray(pointcloud.Color));
    
    % Get interesting points within HSV range.
    interesting_points = find(hsv_points(:, 1) >= hue_range(1) & hsv_points(:, 1) <= hue_range(2) & ...
        hsv_points(:, 2) >= saturation_range(1) & hsv_points(:, 2) <= saturation_range(2) & ...
        hsv_points(:, 3) >= value_range(1) & hsv_points(:, 3) <= value_range(2));

    % Extract interesting points from the point cloud.
    pointcloud = select(pointcloud, interesting_points);
end