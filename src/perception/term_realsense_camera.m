function term_realsense_camera()
    global realsense_camera_pipe;

    % Stop stream.
    realsense_camera_pipe.stop();
    
    fprintf("Closing Realsense Camera stream.\n");
end