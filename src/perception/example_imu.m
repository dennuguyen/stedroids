function example_imu()
    %% IMU Initialisation
    a = arduino('COM3', 'Uno', 'Libraries', 'I2C');
    b = bno055(a, 'OperatingMode', 'ndof');

    %% IMU Calibration
    accelerometer_calibrated_flag = 0;
    gyroscope_calibrated_flag = 0;
    fprintf('Calibrating BNO055\n');
    while (prod([accelerometer_calibrated_flag, gyroscope_calibrated_flag]) ~= 1)
        if (isequal(accelerometer_calibrated_flag, 0) && strcmpi(b.readCalibrationStatus.Accelerometer, "full"))
            accelerometer_calibrated_flag = 1;
            fprintf('Accelerometer calibrated\n');
        end
        if (isequal(gyroscope_calibrated_flag, 0) && strcmpi(b.readCalibrationStatus.Gyroscope, "full"))
            gyroscope_calibrated_flag = 1;
            fprintf('Gyroscope calibrated\n');
        end
    end
    fprintf('BNO055 calibrated\n');
    
    %% Prepare Figures

    % Provide time frame in seconds
    senseFrame = 60; 
    % Measure approximate execution time of a single read cycle
    tic;
    readOrientation(b);
    readAcceleration(b);
    readAngularVelocity(b);
    tDelta = toc;
    % Number of samples to be collected in the senseFrame time frame
    numSamples = floor(senseFrame/tDelta); 
    % Time vector
    tVector = linspace(0, senseFrame, numSamples); 
    tCorrection = 0;
    
    subplot(4, 1, 1)
    hold on
    % Create handle to Azimuth animatedline object
    hAzimuth = animatedline('color', 'r', 'linewidth', 1.25); 
    % Create handle to Pitch animatedline object
      hPitch = animatedline('color', 'k', 'linewidth', 1.25); 
    % Create handle to Roll animatedline object
       hRoll = animatedline('color', 'b', 'linewidth', 1.25); 
    legend('Azimuth (rad)','Pitch (rad)','Roll (rad)');
    ylabel('Euler Angles (rad)');xlabel('Time (s)');
    title('Reading Orientation of the BNO055 sensor', 'fontsize', 12);
    axis([0 senseFrame -6.5 6.5])
    grid minor
    hold off

    subplot(4, 1, 2)
    hold on
    % Create handle to X-axis acceleration animatedline object
    hAx = animatedline('color', 'r', 'linewidth', 1.25); 
    % Create handle to Y-axis acceleration animatedline object
    hAy = animatedline('color', 'k', 'linewidth', 1.25); 
    % Create handle to Z-axis acceleration animatedline object
    hAz = animatedline('color', 'b', 'linewidth', 1.25); 
    legend('A_x (m/s^2)','A_y (m/s^2)','A_z (m/s^2)');
    ylabel('Acceleration (m/s^2)');xlabel('Time (s)');
    title('Reading Accelerometer values from BNO055 sensor', 'fontsize', 12);
    axis([0 senseFrame -30 30]);
    hold off
    grid minor

    subplot(4, 1, 3)
    % Create handle to X-axis angular velocity animatedline object
    hVx = animatedline('color', 'r', 'linewidth', 1.25); 
    % Create handle to Y-axis angular velocity animatedline object
    hVy = animatedline('color', 'k', 'linewidth', 1.25); 
    % Create handle to Z-axis angular velocity animatedline object
    hVz = animatedline('color', 'b', 'linewidth', 1.25); 
    legend('\omega_x (rad/s)','\omega_y (rad/s)','\omega_z (rad/s)');
    ylabel('Angular Velocity (rad/s)');xlabel('Time (s)');
    title('Reading Angular velocity values from BNO055 sensor', 'fontsize', 12);
    axis([0 senseFrame -10 10]);
    hold off
    grid minor

    
    %% Read Data
    for i = 1:100
        [orientation, ~] = readOrientation(b); % Euler angles.
        [acceleration, ~] = readAcceleration(b);
        [angular_velocity, ~] = readAngularVelocity(b);
        [magnetic_field_strength, ~] = readMagneticField(b);
        
        tic;
        addpoints(hAzimuth, tVector(i) + tCorrection, orientation(1));
        addpoints(hPitch, tVector(i) + tCorrection, orientation(2));
        addpoints(hRoll, tVector(i) + tCorrection, orientation(3));

        addpoints(hAx, tVector(i) + tCorrection, acceleration(1));
        addpoints(hAy, tVector(i) + tCorrection, acceleration(2));
        addpoints(hAz, tVector(i) + tCorrection, acceleration(3));

        addpoints(hVx, tVector(i) + tCorrection, angular_velocity(1));
        addpoints(hVy, tVector(i) + tCorrection, angular_velocity(2));
        addpoints(hVz, tVector(i) + tCorrection, angular_velocity(3));

        tCorrection = toc;

        drawnow;
    end
    %% IMU Destruction
    release(b);
    clear a;
    clear b;
end