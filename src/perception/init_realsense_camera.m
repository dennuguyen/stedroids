function init_realsense_camera()
    % realsense_camera_pipe cannot be used in Simulink because it is returned
    % as an mxArray datatype. Therefore, it is given global scope so it exists
    % in the base workspace and not Simulink environment.
    global realsense_camera_pipe;

    % Pipe the stream.
    realsense_camera_pipe = realsense.pipeline();

    % Start stream.
    realsense_camera_pipe.start();
    
    fprintf("Opening Realsense Camera stream.\n");

    % Discard first few frames.
    for i = 1:5
        realsense_camera_pipe.wait_for_frames();
    end
end