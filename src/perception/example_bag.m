function example_bag()
    % Tell pipeline to stream from the given rosbag file.
    filename = 'vid.bag';
    cfg = realsense.config();
    validateattributes(filename, {'char','string'}, {'scalartext', 'nonempty'}, '', 'filename', 1);
    cfg.enable_device_from_file(filename);

    % Pipe the stream.
    pipe = realsense.pipeline();

    % Start stream.
    pipe.start(cfg);
    
    % Stop stream.
    pipe.stop();
end