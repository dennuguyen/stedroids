function example_config()
    % Create context object.
    context = realsense.context();
    devices = context.query_devices();
    device = devices{1};
    
    % Check if camera supports advanced mode.
    if ~device.is('advanced_mode')
        error('Device does not support advanced mode');
    end
    
    % Toggle advanced mode.
    advanced_device = device.as('advanced_mode');
    if ~advanced_device.is_enabled()
        advanced_device.toggle_advanced_mode(true);
        old_state = pause('on');
        pause(2);
        pause(old_state);

        devices = context.query_devices();
        device = devices{1};
        advanced_device = device.as('advanced_mode');

        if ~advanced_device.is_enabled()
            error('Device did not enter advanced mode');
        end
    end

    % Upload config json.
    file_id = fopen('d435_config.json');
    json_string = char(fread(file_id, inf)');
    fclose(file_id);
    advanced_device.load_json(json_string);

    % Pipe the stream.
    pipe = realsense.pipeline();

    % Start stream.
    pipe.start();

    % Discard first few frames.
    for i = 1:5
        pipe.wait_for_frames();
    end

    % Video player to view the frames.
    player = pcplayer([-4 4], [-4 4], [-4 4]);

    while isOpen(player)
        % Create a point cloud object.
        pointcloud = realsense.pointcloud();

        % Get the frame.
        frame = pipe.wait_for_frames();
        depth_frame = frame.get_depth_frame();

        % Get the point cloud from the depth frame.
        points = pointcloud.calculate(depth_frame).get_vertices();
        pointcloud = pointCloud(points);
        view(player, pointcloud);
    end

    % Stop stream.
    pipe.stop();
end