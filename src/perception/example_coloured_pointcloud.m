function example_coloured_pointcloud()
    % Pipe the stream.
    pipe = realsense.pipeline();

    % Start stream.
    pipe.start();

    % Discard first few frames.
    for i = 1:5
        pipe.wait_for_frames();
    end

    % Video player to view the frames.
    player = pcplayer([-1 1], [-1 1], [-1 1]);

    while isOpen(player)
        % Create a point cloud object.
        pointcloud = realsense.pointcloud();
        
        % Get the frame.
        frame = pipe.wait_for_frames();

        % Align the Realsense's cameras to the RGB camera then filter out the
        % frame.
        aligned_frame = realsense.align(realsense.stream.color).process(frame);

        % Split the frame into depth and colour frames.
        depth_frame = aligned_frame.get_depth_frame();
        colour_frame = aligned_frame.get_color_frame();

        % Get the point cloud from the depth frame.
        points = pointcloud.calculate(depth_frame).get_vertices();

        % Get the colour data from the colour frame.
        colour_data = colour_frame.get_data();
        colours = [colour_data(1:3:end)', colour_data(2:3:end)', colour_data(3:3:end)'];

        % Convert realsense.pointcloud to pointCloud datatype to use CV
        % toolbox and keep depth and colour data together.
        pointcloud = pointCloud(points, 'Color', colours);

        % Display the coloured point cloud.
        view(player, pointcloud);
    end
    
    % Stop stream.
    pipe.stop();
end

