# Stedroids

## Dependencies

- [Navigation Toolbox](https://au.mathworks.com/products/navigation.html)
- [Computer Vision Toolbox](https://www.mathworks.com/products/computer-vision.html)
- [Robotics System Toolbox](https://au.mathworks.com/products/robotics.html)
- [MATLAB Support Package for Arduino Hardware](https://au.mathworks.com/help/supportpkg/arduino/ug/install-support-for-arduino-hardware.html)
- [Simulink Support Package for Arduino Hardware](https://au.mathworks.com/help/supportpkg/arduino/ug/install-support-for-arduino-hardware.html)
- [librealsense](https://github.com/IntelRealSense/librealsense/tree/master/wrappers/matlab)
